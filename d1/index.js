//alert("Hello!");

/*
	Selection Control Structures 
		-sorts out whether the statement/s are to be executed based on the condition whether it is true or false

		//if else statement
		//switch statement
		//try catch finally statement
	
	if else statement

	Syntax:
		if(condition) {
			statement
		} else {
			statement
		}

*/

//if statement
// - executes a statement if a specified condition is true
// - can stand alone even without the else statement
/*
	Syntax:
		if(condition) {
			statement//code block
		}
*/

let numA = -1;
// < - less than
// > - greater than
// <= - less than or equal
// >= - greater than or equal

//if statement
if(numA < 0) {
	console.log("Hello");
}
//The result of the expression added in the if condition must result to true, else, the statement inside if() will not run.

console.log(numA < 0);

if(numA > 0) {
	console.log("This statement will not be printed!");
}

console.log(numA > 0);

//Another example
let city = "New York"

if(city === "New York") {
	console.log("Welcome to New York City");
}

//else if
/*
	- Executes a statement if previous conditions are false and if the specified condition is true.
	- The "else if" clause if optional and can be added to capture additional conditions to change the flow of a program
*/

let numB = 1;

if(numA > 0) {
	console.log("Hello");
} else if(numB > 0) {
	console.log("World");
}

//We were able to run the else if statement after we evaluated that the if condition was failed.
//If the if() condition was passed and run, we will no longer evaluate the else if () and end the process there.

//Another Example
city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
} else if( city === "Tokyo") {
	console.log("Welcome to Tokyo");
}


//else statement
/*
	- Executes a statement if all other conditions are false
	- The "else" statement is optional and can be added to capture any other result to change the flow of a program

*/

if( numA > 0) {
	console.log("Hello");
} else if(numB === 0) {
	console.log("World");
} else {
	console.log("Again");
}


//Another example
let age = 20;

if (age <= 17) {
	console.log("Not allowed to drink!");
} else {
	console.log("Matanda ka na, shot na!");
}

/*
	Mini-Activity: 2:50PM
		Create a conditional statement that if height is below 150, display "Did not passed min height req."

		If above above or equal to 150, display "Passed the minimum height req."
		
		**stretch goal: Put it inside a function.
*/

//solution 1
let height = 160;

if (height < 150) {
	console.log("Did not passed min height req.")
} else {
	console.log("Passed the minimum height req.")
}

//solution 2:
function heightReq(h) {
	if (h < 150) {
		console.log("Did not passed min height req.")
	} else {
		console.log("Passed the minimum height req.")
	}
}

heightReq(140);
heightReq(150);
heightReq(175);



//if, else if and else statements with functions
let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		return 'Not a typhoon yet.'
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected.'
	} else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.'
	} else if(windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected.'
	} else {
		return 'Typhoon detected.'
	}
}

//Return the string to the variable "message" that invoked it
message = determineTyphoonIntensity(70);
console.log(message);

if(message === "Tropical storm detected.") {
	console.warn(message);
	console.error(message);
}

//Truthy and Falsy
/*
	-In JavaScript a "truthy" value is a value that is considered TRUE when encountered in a Boolean context
	-Values are considered true unless defined otherwise:
	-Falsy values/exception for Truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN

*/

//Truthy Examples
if(true) {
	console.log("Truthy");
}

if(1) {
	console.log("Truthy");
}

if([]) {
	console.log("Truthy");
}

//Falsy Examples
if(false) {
	console.log("Falsy");
}

if(0) {
	console.log("Falsy");
}

if(undefined) {
	console.log("Falsy");
}


//Conditional (Ternary) Operator
/*
	Syntax:
		(expression) ? ifTrue : ifFalse;

*/

//Single Statement Execution
//Ternary Operators have an implicit "return" statement, meaning that without the "return" keyword, the resulting expression can be stored in a variable

let ternaryResult = (1 < 18) ? "Statement is True" : "Statement is False";

/*
	if(1 < 18) {
		return true
	} else {
		return false
	}

*/
console.log("Result of Ternary Operator: " + ternaryResult);


// Multiple Statement Execution
let name = prompt("Enter your name:");

function isOfLegalAge() {
	//name = "John";
	return "You are of the legal age."
}

function isUnderAge() {
	//name = "Jane";
	return "You are under the age limit."
}

//parseInt() converts the input received into a number data type
let age1 = parseInt(prompt("What is your age?"));
console.log(age1);
console.log(typeof age1);

let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);


//Switch Statement
//Evaluate an expression and match the expression to a case clause.
//An expression will be compared against different cases. Then, we will be able to run code IF the expression being evaluated matches a case.
//IT is used alternatively from an if-else statement. However, if-else statements provides more complexity in its conditions
//.toLowerCase method is a built-in JS method which converts a string to lowercase.



/*
	Syntax:
		switch(expression) {
			case value: 
				statement;
				break;
			default: 
				statement;
				break;
		}
*/


// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day) {

// 	case "lunes":
// 	case 'monday': //,'lunes' 
// 		console.log("The color of the day is red.");
// 		break;
// 	case 'tuesday':
// 		console.log("The color of the day is orange.");
// 		break;
// 	case 'wednesday':
// 		console.log("The color of the day is yellow.");
// 		break;
// 	case 'thursday':
// 		console.log("The color of the day is green.");
// 		break;
// 	case 'friday':
// 		console.log("The color of the day is blue.");
// 		break;
// 	case 'saturday':
// 		console.log("The color of the day is indigo.");
// 		break;
// 	case 'sunday':
// 		console.log("The color of the day is violet.");
// 		break;
// 	default:
// 		console.log("Please input a valid day.")
// 		break;
// }


//Try Catch Finally Statement
// - "try catch" statements are commonly used for error handling
// - There are instances when the application returns an error/warning that is not necessarily an error in the context of our code
// - These errors are a result of an attempt of the programming language to help developers in creating efficient code
// - They are used to specify a response whenever an exception/error is received
// - It is also useful for debugging code because of the "error" object that can be "caught" when using the try catch statement
// - In most programming languages, an "error" object is used to provide detailed information about an error and which also provides access to functions that can be used to handle/resolve errors to create "exceptions" within our code
// - The "finally" block is used to specify a response/action that is used to handle/resolve errors

function showIntensityAlert(windSpeed) {

//try allows us to run code, if there is an error in the instance of our code, then we will be able to catch that error in our catch statement.
	try {

		//Attempt to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

//the error is passed into the catch statement. Developers usually name the error as error, err or even e
	catch (error) {

		//Catch errors within "try" statement
		//error is an object with methods and properties that describe the error.
		//error.message allows us to access information about the error
		console.log(error)
		console.log(error.message)
	}

	finally {
		//Continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
		alert("Intensity updates will show new alert.");
	}

}

showIntensityAlert(56);